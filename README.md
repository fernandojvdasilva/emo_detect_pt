*Pré-requisitos*

- Anaconda Python 3.*
- Instalar bibliotecas Aelius e MXPost, seguindo as instruções indicadas em libs/Aelius-February-25-2013/Install.pdf


*Testando Modelo SVM com Kernel de Árvore*

Rodar o script main/emo_detect_tk.py. Basta digitar o texto e as emoções serão exibidas.
Você também pode usar esse código como base para integrar a outras ferramentas.