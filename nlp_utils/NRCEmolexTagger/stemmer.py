'''
Created on 4 de jun de 2018

@author: fernando
'''

import nltk

class Stemmer(object):    

    STEMMERS = {'pt': nltk.stem.RSLPStemmer()}

    def __init__(self, lang):
        self.lang = lang
        
        if lang in Stemmer.STEMMERS.keys():
            self.stemmer = Stemmer.STEMMERS[lang]
        else:
            self.stemmer = nltk.stem.SnowballStemmer()
        
        
    def stem(self, word):
        return self.stemmer.stem(word)
        