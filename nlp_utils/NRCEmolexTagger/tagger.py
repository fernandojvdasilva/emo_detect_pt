'''
Created on 2 de jun de 2017

@author: fernando
'''

import pandas as pd
from prompt_toolkit.contrib import regular_languages
from nlp_utils.NRCEmolexTagger.stemmer import Stemmer
import os

class NRCEmolexTagger:
    
    def __init__(self):
        self.load()
        
        self.LANGUAGES = {'en': 'English Word', \
                          'ar': 'Arabic Translation (Google Translate)', \
                          'eu': 'Basque Translation (Google Translate)', \
                          'bn': 'Bengali Translation (Google Translate)', \
                          'ca': 'Catalan Translation (Google Translate)', \
                          'zh_HANS': 'Chinese (simplified) Translation (Google Translate)', \
                          'zh_HANT': 'Chinese (traditional) Translation (Google Translate)', \
                          'da': 'Danish Translation (Google Translate)', \
                          'nl': 'Dutch Translation (Google Translate)', \
                          'eo': 'Esperanto Translation (Google Translate)', \
                          'fi': 'Finnish Translation (Google Translate)', \
                          'fr': 'French Translation (Google Translate)', \
                          'de': 'German Translation (Google Translate)', \
                          'el': 'Greek Translation (Google Translate)', \
                          'gu': 'Gujarati Translation (Google Translate)', \
                          'he': 'Hebrew Translation (Google Translate)', \
                          'hi': 'Hindi Translation (Google Translate)', \
                          'ga': 'Irish Translation (Google Translate)', \
                          'it': 'Italian Translation (Google Translate)', \
                          'ja': 'Japanese Translation (Google Translate)', \
                          'la': 'Latin Translation (Google Translate)', \
                          'mr': 'Marathi Translation (Google Translate)', \
                          'fa': 'Persian Translation (Google Translate)', \
                          'pt': 'Portuguese Translation (Google Translate)', \
                          'ro': 'Romanian Translation (Google Translate)', \
                          'ru': 'Russian Translation (Google Translate)', \
                          'so': 'Somali Translation (Google Translate)', \
                          'es': 'Spanish Translation (Google Translate)', \
                          'su': 'Sudanese Translation (Google Translate)', \
                          'sw': 'Swahili Translation (Google Translate)', \
                          'sv': 'Swedish Translation (Google Translate)', \
                          'ta': 'Tamil Translation (Google Translate)', \
                          'te': 'Telugu Translation (Google Translate)', \
                          'th': 'Thai Translation (Google Translate)', \
                          'tr': 'Turkish Translation (Google Translate)', \
                          'uk': 'Ukranian Translation (Google Translate)', \
                          'ur': 'Urdu Translation (Google Translate)', \
                          'vi': 'Vietnamese Translation (Google Translate)', \
                          'cy': 'Welsh Translation (Google Translate)', \
                          'yi': 'Yiddish Translation (Google Translate)', \
                          'zu': 'Zulu Translation (Google Translate)'}
        
    
    def load(self):
        self.wordlist = pd.read_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)), \
                                                 'NRC-Emotion-Lexicon-v0.92-InManyLanguages-web.csv'), sep=';')
        
    def tag(self, word, language):
        if not language in self.LANGUAGES.keys():
            raise Exception("Non supported language")
        
        #stemmer = Stemmer(language)
        #word = stemmer.stem(word)
        
        #search_result = self.wordlist.loc[self.wordlist[self.LANGUAGES[language]].apply(stemmer.stem) == word]
        
        search_result = self.wordlist.loc[self.wordlist[self.LANGUAGES[language]] == word]
                                          
        tagged_emotions = {'Positive': False,
                            'Negative': False,
                            'Anger': False,
                            'Anticipation': False,
                            'Disgust': False,
                            'Fear': False,
                            'Joy': False,
                            'Sadness': False,
                            'Surprise': False,
                            'Trust': False}
        
        if len(search_result) == 0:
            return None
        
        for emo in tagged_emotions.keys():
            for index, row in search_result.iterrows():
                if row[emo] == 1:
                    tagged_emotions[emo] = True
                    
        return tagged_emotions
    
    def tagNLangs(self, word, language_list):
        tagged_emotions = None
        for lang in language_list:
            tagged_emotions = self.tag(word, lang)
            if tagged_emotions != None:
                break
            
        return tagged_emotions
                
    
    def tagWordList(self, wordList, language_list):
        tagged_words = []
        for w in wordList:
            tagged_words.append(self.tagNLangs(w, language_list))
        
        return tagged_words
            
    
