'''
Created on 24/02/2016

@author: fernando
'''

from nlp_utils.nlp_lexical import *
from nlp_utils.nlp_lexical import lex_remove_punctuations
import re
from nlp_utils.nlp_lexical import pt_stemmer
from nlp_utils.nlp_emotions import *

STOCK_CODES = [ "ABEV3", \
            "AEDU3", \
        "ALLL3", \
        "BBAS3", \
        "BBDC3", \
        "BBDC4", \
        "BBSE3", \
        "BISA3", \
        "BRAP4", \
        "BRFS3", \
        "BRKM5", \
        "BRML3", \
        "BRPR3", \
        "BVMF3", \
        "CCRO3", \
        "CESP6", \
        "CIEL3", \
        "CMIG4", \
        "CPFE3", \
        "CPLE6", \
        "CRUZ3", \
        "CSAN3", \
        "CSNA3", \
        "CTIP3", \
        "CYRE3", \
        "DASA3", \
        "DTEX3", \
        "ECOR3", \
        "ELET3", \
        "ELET6", \
        "ELPL4", \
        "EMBR3", \
        "ENBR3", \
        "ESTC3", \
        "EVEN3", \
        "FIBR3", \
        "GFSA3", \
        "GGBR4", \
        "GOAU4", \
        "GOLL4", \
        "HGTX3", \
        "HYPE3", \
        "ITSA4", \
        "ITUB4", \
        "JBSS3", \
        "KLBN11",\
        "KLBN4", \
        "KROT3", \
        "LAME4", \
        "LIGT3", \
        "LLXL3", \
        "LREN3", \
        "MRFG3", \
        "MRVE3", \
        "NATU3", \
        "OIBR4", \
        "PCAR4", \
        "PDGR3", \
        "PETR3", \
        "PETR4", \
        "QUAL3", \
        "RENT3", \
        "RSID3", \
        "SANB11", \
        "SBSP3", \
        "SUZB5", \
        "TBLE3", \
        "TIMP3", \
        "UGPA3", \
        "USIM5", \
        "VALE3", \
        "VALE5", \
        "VIVT4"]

STOCK_CODE_TOKEN = 'stock'
URL_TOKEN = 'url'
CURRENCY_TOKEN = 'currency'
PERCENTAGE_TOKEN = 'percentage'

URL_REGEX = r'https?:\/\/[\/a-zA-Z0-9\.\?\&\=]*'
CURRENCY_REGEX = r'([rR]{1}\$\s)?[0-9]{1,5}[\.,][0-9]{2}'
PERCENTAGE_REGEX = r'\.?[0-9]{1,3}([\.,][0-9]{1,3})?\s?\%'




def sem_tok_repl(sentence, regex, token_repl):
    re_obj = re.search(regex, sentence)
    
    while not re_obj is None:
        sentence = sentence.replace(re_obj.group(0), token_repl)
        re_obj = re.search(regex, sentence)
    
    return sentence
    
def sem_tok_stock_code(token):
    for code in STOCK_CODES:
        if code.lower() == token.lower():
            return STOCK_CODE_TOKEN
    return token

def sem_is_stock_code(token):
    for code in STOCK_CODES:
        if code.lower() == token.lower():
            return True
    return False

def sem_tokenize_stock_corpus(sentence):
    sentence = emo_remove_hashtags(sentence)
    sentence = sem_tok_repl(sentence, URL_REGEX, URL_TOKEN)
    sentence = sem_tok_repl(sentence, CURRENCY_REGEX, CURRENCY_TOKEN)
    sentence = sem_tok_repl(sentence, PERCENTAGE_REGEX, PERCENTAGE_TOKEN)
    
    tokens = lex_tokenize(sentence)
    tokens = [sem_tok_stock_code(tok) for tok in tokens]        
    tokens = lex_stopwords_removal(tokens)
    tokens = [lex_remove_punctuations(tok) for tok in tokens]
        
    stems = lex_stem(tokens, pt_stemmer)
    
    return stems

def sem_tokenize_stock_corpus_no_stem(sentence):
    sentence = emo_remove_hashtags(sentence)
    sentence = sem_tok_repl(sentence, URL_REGEX, URL_TOKEN)
    sentence = sem_tok_repl(sentence, CURRENCY_REGEX, CURRENCY_TOKEN)
    sentence = sem_tok_repl(sentence, PERCENTAGE_REGEX, PERCENTAGE_TOKEN)
    
    tokens = lex_tokenize(sentence)
    tokens = [sem_tok_stock_code(tok) for tok in tokens]        
    tokens = lex_stopwords_removal(tokens)
    tokens = [lex_remove_punctuations(tok) for tok in tokens]        
    
    return tokens