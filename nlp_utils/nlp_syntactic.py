'''
Created on 10 de ago de 2018

@author: fernando
'''
import sys

# Aelius path configuration
sys.path.append("/home/fernando/Applications/Aelius")

from Aelius import AnotaCorpus, Toqueniza

AELIUS_DATA_DIR = '/home/fernando/aelius_data'

def syn_pos_tag(tokens, lang):
    if lang == 'pt':
        tokens=AnotaCorpus.anota_sentencas([tokens],modelo=AELIUS_DATA_DIR, arquitetura='mxpost')

    return tokens
