cd ../main
#python tweets_dataseries_to_trees.py -p ../datasets/tweets_auto-tagged-emolex_train.csv -o ../datasets/ >../logs/tweets_dataseries_to_trees_auto-tagged-train.log 2>&1 &
#python tweets_dataseries_to_trees.py -p ../datasets/tweets_auto-tagged-emolex_test.csv -o ../datasets/ >../logs/tweets_dataseries_to_trees_auto-tagged-test.log 2>&1 &
#python tweets_dataseries_to_trees.py -p ../datasets/tweets_stocks_emolex.csv -o ../datasets/ >../logs/tweets_dataseries_to_trees_stocks.log 2>&1 &
#python tweets_dataseries_to_trees.py -p ../datasets/full_agreement/tweets_stocks_emolex.csv -o ../datasets/full_agreement >../logs/tweets_dataseries_to_trees_stocks_full_agreement.log 2>&1 &

# Neutral tweets
python tweets_dataseries_to_trees.py -i -n -p ../datasets/tweets_auto-tagged-emolex_test.csv -o ../datasets/ >../logs/tweets_dataseries_to_trees_auto-tagged-test-neutral.log 2>&1 &
python tweets_dataseries_to_trees.py -n -p ../datasets/tweets_stocks_emolex.csv -o ../datasets/ >../logs/tweets_dataseries_to_trees_stocks-neutral.log 2>&1 &
