import os
import pandas as pd
import uuid


class SvmTKIOHandler:  

  def getTree(self):
    pass

  def saveResult(self, predicted):
    pass

  def closeAll(self):
    pass

  def isPredicted(self):
    pass

class Text2CsvIOHandler(SvmTKIOHandler):

  def __init__(self, filepath, out_filepath, configs, skip=0):
    self.in_file = open(filepath, 'r')
    if skip == 0:
      self.out_file = open(out_filepath, 'w+')
      self.out_file.write('y;in_tree')
      for c in configs:
        self.out_file.write(';tk%d' % c)
      self.out_file.write('\n')
    else:
      self.out_file = open(out_filepath, 'a')
    

  def getTree(self):
    self.curr_line = self.in_file.readline()
    self.saved_tree = False
    return self.curr_line

  def saveResult(self, predicted, config):
    if not self.saved_tree:
      self.curr_line = self.curr_line.replace('"', "'")
      self.out_file.write('\n%s;%s' % (self.curr_line[:self.curr_line.find('\t')],  f'"{self.curr_line}"'))  
      self.saved_tree = True
    self.out_file.write(';%f'  % predicted)  

  def closeAll(self):
    self.in_file.close()
    self.out_file.close()

  def isPredicted(self, config):
    return False



class CsvIOHandler(SvmTKIOHandler):

  def __init__(self, filepath):
    self.file_df = pd.read_csv(filepath, sep=';')
    self.curr_index = -1

  def getTree(self):
    try:
      self.curr_index += 1
      result = self.file_df.iloc[curr_index]      
    except:
      self.curr_index = 0
      result = False

    return result['in_tree']
    
  def saveResult(self, predicted, config):
    self.file_df.iloc[curr_index][config] = predicted

  def closeAll(self):
    pass


  def isPredicted(self, config):
    return not self.file_df.iloc[self.curr_index][config] is False


SVM_LIGHT_TK_PATH = 'libs/svm-light-TK-1.2/svm-light-TK-1.2.1/'
SVM_CLASSIFY_BIN_FILENAME = 'svm_classify'

def svmtk_predict(model_filepath, in_tree, svmtk_path=SVM_LIGHT_TK_PATH):

  tmp_input_filename = 'svm_tk_input_' + str(uuid.uuid4()) + '.tmp'
  tmp_input_file = open(tmp_input_filename, 'w+')
  tmp_input_file.write(in_tree)
  tmp_input_file.close()

  tmp_pred_filename = tmp_input_filename.replace('tmp', 'pred.tmp')

  svm_classify_cmd = "%s -v 1 %s %s %s" % (os.path.join(svmtk_path, SVM_CLASSIFY_BIN_FILENAME), \
                                           tmp_input_filename, model_filepath, tmp_pred_filename)


  cmd_result = os.system(svm_classify_cmd)

  os.remove(tmp_input_filename)

  if cmd_result == 0:
    tmp_pred_file = open(tmp_pred_filename, 'r')
    result = float(tmp_pred_file.readline())
    tmp_pred_file.close()
    os.remove(tmp_pred_filename)
  else:
    result = False


  return result


DEBUG_TEST_SVMTK = False
def test_svmtk(model_filename, data_filename, kernels, isCsv=False, skip=0):
    data_filepath = os.path.join('datasets/', data_filename)
    model_filepath = os.path.join('models/', model_filename)

    if isCsv:
      ioHandler = CsvIOHandler(data_filepath)
    else:
      out_filepath = os.path.join('logs', 'results_' + data_filename.replace('txt', 'csv'))
      ioHandler = Text2CsvIOHandler(data_filepath, out_filepath, kernels, skip)

    for i in range(skip):
      ioHandler.getTree()

    num_tested = 0
    num_failed = 0
    curr_tree = ioHandler.getTree()
    if DEBUG_TEST_SVMTK:
      dbg_count = 0
    while curr_tree:
      for i in kernels:
        if len(kernels) > 1:
          curr_model_filepath = model_filepath.replace('BEST_PARS', 'BEST_PARS_KERNEL_%d' % i)
        else:
          curr_model_filepath = model_filepath
        config = 'tk%d' % i
        if not ioHandler.isPredicted(config):
          pred = svmtk_predict(curr_model_filepath, curr_tree)        
          ioHandler.saveResult(pred, config)  
          num_tested += 1
          if not pred:
            num_failed += 1
      curr_tree = ioHandler.getTree()

      if DEBUG_TEST_SVMTK:
        dbg_count += 1
        if dbg_count == 20:
          break

    ioHandler.closeAll()

    return num_tested, num_failed

# Testing one vs one models
#for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:
#for emo in [('JOY', 'SAD')]:  
#  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
#                                      'tweets_auto-tagged-emolex_test_svmlight_%s_vs_%s.txt' % emo, \
#                                      [-1])

# Testing neutral

#for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:  
#  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
#                                      'tweets_auto-tagged-emolex_test_svmlight_%s_vs_%s_neutral.txt' % emo, \
#                                      [-1])


#  print("%s vs %s: %d tested, %d failed" % (emo[0], emo[1], num_tested, num_failed))

# Testing one vs one models for stocks
#for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:
#for emo in [('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:  
#  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
#                                      'tweets_stocks_emolex_svmlight_%s_vs_%s.txt' % emo, \
#                                      [-1])

#for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:  
#for emo in [('JOY', 'SAD')]:  
#  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
#                                      'tweets_stocks_emolex_svmlight_%s_vs_%s.txt' % emo, \
#                                      [-1])


# Testing neutral
#for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:  
#  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
#                                      'tweets_stocks_emolex_svmlight_%s_vs_%s_neutral.txt' % emo, \
#                                      [-1])

#  print("%s vs %s: %d tested, %d failed" % (emo[0], emo[1], num_tested, num_failed))

'''
# Testing one vs all models
for emo in ['JOY', 'SAD', 'TRU', 'DIS', 'ANT', 'SUR', 'ANG', 'FEA']:
  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_ALL_vs_%s_BEST_PARS.txt' % emo, \
                                      'tweets_auto-tagged-emolex_test_svmlight_ALL_vs_%s.txt' % emo, \
                                      [-1])

  print("ALL vs %s: %d tested, %d failed" % (emo, num_tested, num_failed))

# Testing one vs all models for stocks
for emo in ['JOY', 'SAD', 'TRU', 'DIS', 'ANT', 'SUR', 'ANG', 'FEA']:
  num_tested, num_failed = test_svmtk('classifier-tree-kernel-model_ALL_vs_%s_BEST_PARS.txt' % emo, \
                                      'tweets_stocks_emolex_svmlight_ALL_vs_%s.txt' % emo, \
                                      [-1])

  print("ALL vs %s: %d tested, %d failed" % (emo, num_tested, num_failed))
'''