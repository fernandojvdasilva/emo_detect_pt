#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Converts tweets from a dataseries CSV file to trees format, according to the
svm-light-TK format

@author:     Fernando J. V. da Silva

@copyright:  2016 Fernando J. V. da Silva. All rights reserved.

@license:    GPL

@contact:    fernandojvdasilva@gmail.com
'''

import sys
import os
import string

import pandas as pd

sys.path.append(os.path.join(os.getcwd(), '../'))

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from tweets.tweet_corpus import TweetCorpus
from tweets_annotator_manual.tweet_annotation_manual import TweetAnnotationManual
from tweets_annotator_manual.tweets_annotator_config import AnnotatorConfig
from tweets.tweets_filter_repeated import TweetsFilterRepeated
from experiments.experiments_utils import *
from nlp_utils.nlp_emotions import *


from tweet_tree.tweet_tree import TweetTree
from tweet_tree.parser_factory import ParserFactory

__all__ = []
__version__ = 0.1
__date__ = '2018-09-09'
__updated__ = '2018-09-09'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

header = ''



def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = ""
    program_license = '''%s

  Created by Fernando J. V. da Silva on %s.
  Copyright 2016 Fernando J. V. da Silva. All rights reserved.

  Licensed under GPL

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

#    try:
    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-p", "--path", dest="path", help="Dataseries file path" )    
    parser.add_argument("-o", "--out", dest="out", help="Output directory" )
    parser.add_argument('-V', '--version', action='version', version=program_version_message)
    parser.add_argument('-i', '--ignore-all-neutral', dest='ignore_all_neutral', action='store_true', help="Ignore samples with all emotions neutral (default False)")
    parser.add_argument('-n', '--save-neutral', dest='save_neutral', action='store_true', help="Save neutral, instead of emotional samples")


    # Process arguments
    args = parser.parse_args()

    out_dir = args.out
    
    path = args.path

    ignore_all_neutral = args.ignore_all_neutral

    save_neutral = args.save_neutral

    # DEBUG ONLY
    #out_dir = '../datasets/'
    #path = '../datasets/tweets_auto-tagged-emolex_test.csv'


    df = pd.read_csv(path, engine='c')
    
    emotions = [('ANT', 'SUR'), ('TRU', 'DIS'), ('ANG', 'FEA'), ('JOY', 'SAD')]
    
    tweet_tree_parser = ParserFactory.createParseLayers()
    tweet_tree = TweetTree(tweet_tree_parser)
    
    
    #df['tree'] = df.apply(lambda row: tweet_tree.parse_text(row['text'])[0].getParseTree(), axis=1)
    
    emo_files = {}
    neutral_files = {}
    for emo in emotions:
        out_filename = path.replace('.csv', '_svmlight_%s_vs_%s.txt' % emo)
        neutral_filename = path.replace('.csv', '_svmlight_%s_vs_%s_neutral.txt' % emo)
        emo_files["%s_vs_%s" % emo] = open(out_filename, 'w')
        neutral_files["%s_vs_%s" % emo] = open(neutral_filename, 'w')
        
    
    for emo in emotions:
        
        print('Saving emotion pair file %s_vs_%s' % emo)                
         
        
        if save_neutral:
            if ignore_all_neutral:
                emo_class, emo_ind = extract_neutral_n_class(df, emo[0], emo[1])
            else:
                emo_class, emo_ind = extract_neutral_n_class_and_with_all_neutral(df, emo[0], emo[1])

            out_file = neutral_files["%s_vs_%s" % emo]
        else:
            emo_class, emo_ind = extract_emos_n_class(df, emo[0], emo[1])
            out_file = emo_files["%s_vs_%s" % emo]

        df_emo = df.loc[emo_ind, :]
        print('Shape: ')    
        print(df_emo.shape)
       
        for index, row in df_emo.iterrows():                       
            try:
                sen = emo_remove_hashtags(emo_remove_hashtags(row['text']))
                str_tree = tweet_tree.parse_text(sen)[0].getParseTree()
                out_file.write('%d\t|BT| %s |ET|\n' % (1 if row[emo[1]] == 1 else -1, str_tree))
                
                '''
                for emo_ in emotions:
                    if emo_ != emo and row[emo_[0]] == row[emo_[1]] == 0:
                        neutral_file = neutral_files["%s_vs_%s" % emo_]
                        try:                
                            neutral_file.write('0\t|BT| %s |ET|\n' % str_tree)
                        except:
                            print("Failed tree (neutra file) : %s\n" % str_tree)

                '''                            
                        
            except:
                print("Failed tree: %s\n" % row['text'])
            
       
        out_file.close()
        
    for emo in emotions:
        neutral_files["%s_vs_%s" % emo].close()                    

    print("Done!")

    return 0
#     except KeyboardInterrupt:
#         ### handle keyboard interrupt ###
#         return 0
#     except Exception, e:
#         if DEBUG or TESTRUN:
#             raise(e)
#         indent = len(program_name) * " "
#         sys.stderr.write(program_name + ": " + repr(e) + "\n")
#         sys.stderr.write(indent + "  for help use --help")
#         return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'tools.tweets_annotation_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
