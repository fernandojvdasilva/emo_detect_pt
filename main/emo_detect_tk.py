import os
import sys

sys.path.append(os.path.join(os.getcwd(), '../'))

from experiments.test_tree_classifier import *
from tweet_tree.tweet_tree import TweetTree
from tweet_tree.parser_factory import ParserFactory

import pickle

#from pudb import set_trace

#set_trace()


thresholds = pickle.load(open('../models/probs_train_neutral_tk.pkl', 'rb'))

print("Text (in Portuguese):")
text = input()

print('* Tree parsing...')
tweet_tree_parser = ParserFactory.createParseLayers()
tweet_tree = TweetTree(tweet_tree_parser)

print('Tree representation:')

str_tree = '\t|BT| %s |ET|\n' % tweet_tree.parse_text(text)[0].getParseTree()

print(str_tree)

SVM_LIGHT_TK_PATH = '../libs/svm-light-TK-1.2/svm-light-TK-1.2.1/'

print('* Classifying emotions...')
for emo in [('JOY', 'SAD'), ('TRU', 'DIS'), ('ANT', 'SUR'), ('ANG', 'FEA')]:	
	print('Scores: === %s (<= %.2f) ===|=== NEUTRAL ===|=== %s (>= %.2f) ===' % (emo[0], thresholds['%s-vs-%s' % emo][emo[1]]['threshold'], \
																	  emo[1],
																	  thresholds['%s-vs-%s' % emo][emo[0]]['threshold']))
	emo_score = svmtk_predict('../models/classifier-tree-kernel-model_%s_vs_%s_BEST_PARS.txt' % emo, \
		str_tree, SVM_LIGHT_TK_PATH)
	
	if emo_score > thresholds['%s-vs-%s' % emo][emo[1]]['threshold'] and \
	emo_score < thresholds['%s-vs-%s' % emo][emo[0]]['threshold']:
		print('Neutral for %s vs %s' % emo  + '(score %.2f)' % emo_score)
	else:
		if emo_score > 0:
			print(emo[1] + '(score %.2f)' % emo_score)
		else:
			print(emo[0] + '(score %.2f)' % emo_score)


	'''if emo_score <= thresholds['%s-vs-%s' % (emo[1], emo[0])][emo[1]]['threshold']:
		print(emo[1] + '(score %.2f)' % emo_score)
	elif emo_score >= thresholds['%s-vs-%s' % (emo[1], emo[0])][emo[0]]['threshold']:
		print(emo[0] + '(score %.2f)' % emo_score)
	else:
		print('Neutral for %s vs %s' % (emo[1], emo[0])  + '(score %.2f)' % emo_score)

	print(emo_score)
	'''