'''
Created on 26 de mai de 2018

@author: fernando
'''

from tweet_tree.parse_emo_nodes import ParseLayerEmo
from tweet_tree.parse_emoticon_nodes import ParseLayerEmoticons
from tweet_tree.parse_exclamation_nodes import ParseLayerExclamation
from tweet_tree.parse_interrogation_nodes import ParseLayerInterrogation
from tweet_tree.parse_negative_nodes import ParseLayerNegation
from tweet_tree.parse_root import ParseLayerRoot
from tweet_tree.parse_stopword_nodes import ParseLayerStopword
from tweet_tree.parse_target_nodes import ParseLayerTarget
from tweet_tree.parse_string_nodes import ParseLayerStringNodes
from tweet_tree.parse_url_nodes import ParseLayerUrl
from tweet_tree.parse_num_nodes import ParseLayerNum
from tweet_tree.parse_hashtag_nodes import ParseLayerHashtag
from tweet_tree.parse_pos_nodes import ParsePOS

class ParserFactory(object):


    @staticmethod
    def createParseLayers():
        root_layer = ParseLayerRoot(None)
        str_layer = ParseLayerStringNodes(root_layer)        
        hash_layer = ParseLayerHashtag(str_layer) # OK
        exc_layer = ParseLayerExclamation(hash_layer) # OK
        int_layer = ParseLayerInterrogation(exc_layer) # OK 
        targ_layer = ParseLayerTarget(int_layer) # OK 
        neg_layer = ParseLayerNegation(targ_layer) # OK 
        stopw_layer = ParseLayerStopword(neg_layer) # OK 
        emot_layer = ParseLayerEmoticons(stopw_layer)
        emo_layer = ParseLayerEmo(emot_layer)        
        url_layer = ParseLayerUrl(emo_layer) # OK
        num_layer = ParseLayerNum(url_layer) # OK
        pos_layer = ParsePOS(num_layer)
        
        return pos_layer