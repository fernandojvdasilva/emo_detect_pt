'''
Created on 26 de mai de 2018

@author: fernando
'''
import unittest

import sys
import os
#sys.path.append("/home/fernandojvdasilva/phd_project")
#sys.path.append("/home/fernandojvdasilva/phd_project/tweet_tree")
#sys.path.append("/home/fernando/UNICAMP/PHD/src/phd_project")
#sys.path.append("/home/fernando/UNICAMP/PHD/src/phd_project/tweet_tree")
sys.path.append(os.path.join(os.getcwd(), '../'))



from tweet_tree.tweet_tree import TweetTree
from tweet_tree.parser_factory import ParserFactory
from nlp_utils.nlp_lexical import lex_tokenize

from nltk.draw import TreeView
from nltk.draw import TreeWidget
from nltk.draw.util import CanvasFrame
from nltk import Tree

class Test(unittest.TestCase):

    def ignoreCharNotSupportedByTkinter(self, text):
        char_list = [text[j] if ord(text[j]) in range(65536) else '*' for j in range(len(text))]
        text=''
        for j in char_list:
            text=text+j
                        
        return text
    
    __num_tests = 0
    def num_tests(self):
        self.__num_tests += 1
        return self.__num_tests 

    def showTree(self, treestr):
        # Ignore characters not supported by Tkinter (visualization only)
        treestr = self.ignoreCharNotSupportedByTkinter(treestr)        
        
        t = Tree.fromstring(treestr)

        TreeView(t)
        
        cf = CanvasFrame()
        tc = TreeWidget(cf.canvas(),t)
        cf.add_widget(tc,10,10) # (10,10) offsets
        cf.print_to_file("%d.ps" % self.num_tests())
        cf.destroy()

    def testTreeUnderscore(self):
        tweet_tree_parser = ParserFactory.createParseLayers()
        tweet_tree = TweetTree(tweet_tree_parser)
        
        sample = '''tente ser #feliz ao inves dee tentaar ser #perfeito.
 #bouuah noithe.'''
        sample = emo_remove_hashtags(sample)        
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)
        
    def testTreeEndHashtag(self):
        tweet_tree_parser = ParserFactory.createParseLayers()
        tweet_tree = TweetTree(tweet_tree_parser)
        
        sample = 'ao - alberto oliveira: georgia  (leo) https://t.co/dzbuububs5  #diversão #feliz #cultura #albertooliveira #alberto #conto #poesia #ator #'
        sample = emo_remove_hashtags(sample)
        print(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)        


    def testBadlyFormedNOTTree(self):
        tweet_tree_parser = ParserFactory.createParseLayers()
        tweet_tree = TweetTree(tweet_tree_parser)

        sample = '@rodrigo_moller_ @jandira_feghali como quer ser prefeita c ñ tem nem compostura chega o br não merece e nem precisa d políticos assim #nojo😡'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)

        assert(not '()' in str_tree)

    def testSimpleTree(self):        
        tweet_tree_parser = ParserFactory.createParseLayers()
        tweet_tree = TweetTree(tweet_tree_parser)
        
        sample = 'oi sexta  #feriadosohpraquempode #pqeutotrabalhando #chateada 😔  #boatarde #instagood #instapicture… https://t.co/rclxr1g0wn'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)
                
        sample = 'rt da gi passando na tl a essa hora 😨 #medo 🏃🏃'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)
        
        # TODO: Handle repeated emoticons or punctuation
        # TODO: Handle concatenated words        
        # TODO: Handle slangs like "pq" "soh"
                
        sample = 'que feijão mais gostoso 😋😋😋 #cariocando #carioquice #errejota #feijão  #delícia #todosujo… https://t.co/pyzneoguks'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)        
        assert(res != None)
        
        sample = 'vizinho das plantas foi embora 😯 #luto #acabouotráfico #acaboubriganamadruga'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)

        sample = 'feriado com meu amor!❤ 😎 🍀💏🙏 #melhorcompanhia #amodemais #quandodeusquer #feliz #nossasenhoraabençoe https://t.co/ajcllvkkwi'
        sample = emo_remove_hashtags(sample)
        res = tweet_tree.parse_text(sample)
        str_tree = res[0].getParseTree()
        print(str_tree)
        self.showTree(str_tree)
        assert(res != None)


if __name__ == "__main__":
    import sys;sys.argv = ['', 'Test.testTreeUnderscore', 'Test.testTreeEndHashtag']
    unittest.main()
