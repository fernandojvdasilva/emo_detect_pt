'''
Created on 2 de jun de 2017

@author: fernando
'''

from tweet_tree.tweet_tree import ParseLayer, TweetTreeNode
from nlp_utils.nlp_emotions import emo_tag_emoticons

class ParseLayerEmoticons(ParseLayer):
    
    def parse(self, nodes):
        res_nodes = []
        
        emo_tags = emo_tag_emoticons([node.content for node in nodes])
        
        for emo, node in zip(emo_tags, nodes):
            
            node_content = node.content
            if node_content in TweetTreeNode.TAGS.keys():
                res_nodes.append(node)
                continue
            
            word_node = node
            has_emotion = False
            
            #res_nodes.append(word_node)
            
            if emo != None:
                emo_children = []
                for key in emo.keys():
                    if emo[key] is True:
                        emo_children.append(TweetTreeNode(key))
            
                if len(emo_children) > 0:
                    emo_node = TweetTreeNode('EM')
                    
                    for c in emo_children:
                        emo_node.addChild(c)
                        
                    emotion_word_node = TweetTreeNode('ET')
                    
                    emotion_word_node.addChild(word_node)
                    emotion_word_node.addChild(emo_node)
                    
                    res_nodes.append(emotion_word_node)
                
                    has_emotion = True
            
            if not has_emotion:
                res_nodes.append(node)
        
        return self.nextLayer.parse(res_nodes)
